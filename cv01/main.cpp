#include <iostream>
using namespace std;

class Majetek{
public:
    string m_popis;
    string m_id;
    float m_porizovaciCena;
    string m_datumOdpisu;

    void printAll(){
        cout << "Popis je: " << m_popis << endl;
        cout << "ID je: " << m_id << endl;
        cout << "Cena je: " << m_porizovaciCena << endl;
        cout << "Datum odpisu: " << m_datumOdpisu << endl;
    }

    void setPopis(string novyPopis){
        m_popis = novyPopis;
    }

    string getPopis(){
        return m_popis;
    }
};

int main() {
    Majetek* pocitac = new Majetek;
    pocitac->m_porizovaciCena = 10000.5;
    pocitac->m_popis = "HP Proliant 987";
    pocitac->m_datumOdpisu = "28.8.2020";
    pocitac->m_id = "N50001";
    pocitac->printAll();

    string popisPocitace = pocitac->getPopis();
    cout << popisPocitace << endl;
    
    delete pocitac;
    return 0;
}