//
// Created by xproch17 on 09.11.2018.
//

#include "Uis.h"

Uis::Uis(){

}

Uis::~Uis(){
    for(auto ucitel:m_ucitele){ //Ucitel*
        delete ucitel;
    }
}

void Uis::addUcitel(std::string jmeno, std::string rodneCislo, std::string ustav){
    Ucitel* novyUcitel = new Ucitel(jmeno, rodneCislo, ustav);
    m_ucitele.push_back(novyUcitel);
}

void Uis::printUcitele(){
    for(auto ucitel:m_ucitele){ //Ucitel*
        ucitel->printInfo();
    }
}