//
// Created by xproch17 on 09.11.2018.
//

#ifndef CV07_OSOBA_H
#define CV07_OSOBA_H

#include <iostream>

class Osoba {
    std::string m_jmeno;
    std::string m_rodneCislo;
public:
    Osoba(std::string jmeno, std::string rodneCislo);
    std::string getJmeno();
    std::string getRodneCislo();
    void setJmeno(std::string jmeno);
    void setRodneCislo(std::string rodneCislo);
};

#endif //CV07_OSOBA_H
