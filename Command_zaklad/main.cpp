#include <iostream>
#include "Osoba.h"
#include "Bojuj.h"
#include "Kradez.h"

int main() {
    Osoba* pepa = new Osoba("Pepa", 10);
    Bojuj* boj = new Bojuj();
    pepa->naucInterakci(boj);

    Kradez* kradez = new Kradez();
    pepa->naucInterakci(kradez);

    Osoba* karel = new Osoba("Karel", 10);
    pepa->interaguj(karel);

    delete kradez;
    delete boj;
    delete pepa;
    delete karel;
    return 0;
}