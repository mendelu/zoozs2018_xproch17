//
// Created by xproch17 on 07.12.2018.
//

#include "Bojuj.h"

Bojuj::Bojuj() : Interakce("Boj"){

}

void Bojuj::interaguj(Osoba* kdo, Osoba* skym){
    int silaSkym = skym->getSila();
    kdo->uberZdravi(silaSkym);

    skym->uberZdravi( kdo->getSila() );

    std::cout << "Probehl souboj" << std::endl;
}