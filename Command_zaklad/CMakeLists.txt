cmake_minimum_required(VERSION 3.12)
project(Command_zaklad)

set(CMAKE_CXX_STANDARD 14)

add_executable(Command_zaklad main.cpp Osoba.cpp Interakce.cpp Interakce.h Bojuj.cpp Bojuj.h Komunikace.cpp Komunikace.h Kradez.cpp Kradez.h)