#include "Osoba.h"
#include "Interakce.h"

Osoba::Osoba(std::string jmeno, int sila){
	m_jmeno = jmeno;
	m_sila = sila;
	m_zdravi = 100;
}

//-----------------------------------
// Sprava veci
//-----------------------------------

void Osoba::seberPredmet(Predmet* predmet){
	m_predmety.push_back(predmet);
}

Predmet* Osoba::getPredmet(int index){
	if ((index >= 0) && (index < m_predmety.size())){
		return m_predmety.at(index);
	} else {
		return nullptr;
	}
}

void Osoba::odeberPredmet(int index){
	if ((index >= 0) && (index < m_predmety.size())){
		m_predmety.erase(m_predmety.begin()+index);
	}
}

//-----------------------------------
// Bezne getry
//-----------------------------------
void Osoba::uberZdravi(int kolik){
	if (m_zdravi > kolik){
		m_zdravi -= kolik;
	} else {
		m_zdravi = 0;
	}
}

int Osoba::getSila(){
	return m_sila;
}

int Osoba::getZdravi(){
	return m_zdravi;
}

std::string Osoba::getJmeno(){
	return m_jmeno;
}


void Osoba::naucInterakci(Interakce* interakce){
    m_interace.push_back(interakce);
}


void Osoba::interaguj(Osoba* druhaOsoba){
/*
    for(Interakce* akce: m_interace){
    	std::cout << akce->getPopis();
    }
*/
	for (int i = 0; i < m_interace.size(); ++i) {
		std::cout << "[" << i << "]: "
		<< m_interace.at(i)->getPopis() << std::endl;
	}

	int rozhodnuti = Komunikace::ziskejRozhodnuti(m_interace.size());
	m_interace.at(rozhodnuti)->interaguj(this, druhaOsoba);
}
