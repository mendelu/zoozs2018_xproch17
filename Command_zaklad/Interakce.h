//
// Created by xproch17 on 07.12.2018.
//

#ifndef COMMAND_ZAKLAD_INTERAKCE_H
#define COMMAND_ZAKLAD_INTERAKCE_H

#include <iostream>
#include "Osoba.h"

class Interakce {
    std::string m_popis;
public:
    Interakce(std::string popis);
    virtual void interaguj(Osoba* kdo, Osoba* skym) = 0;
    std::string getPopis();
};


#endif //COMMAND_ZAKLAD_INTERAKCE_H
