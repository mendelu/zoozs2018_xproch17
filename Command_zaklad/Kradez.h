//
// Created by xproch17 on 07.12.2018.
//

#ifndef COMMAND_ZAKLAD_KRADEZ_H
#define COMMAND_ZAKLAD_KRADEZ_H

#include "Osoba.h"
#include "Interakce.h"

class Kradez : public Interakce{
public:
    Kradez();
    void interaguj(Osoba* kdo, Osoba* skym);
};


#endif //COMMAND_ZAKLAD_KRADEZ_H
