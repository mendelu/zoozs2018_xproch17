#ifndef __Command_Interaction__Osoba__
#define __Command_Interaction__Osoba__

#include <vector>
#include <iostream>
#include "Predmet.h"
#include "Komunikace.h"
//#include "Interakce.h"

class Interakce;

class Osoba{
private:
	std::vector<Predmet*> m_predmety;
	std::vector<Interakce*> m_interace;

	int m_sila;
	int m_zdravi;
	std::string m_jmeno;
public:
	Osoba(std::string jmeno, int sila);
	
	void seberPredmet(Predmet* predmet);
	Predmet* getPredmet(int index);
	void odeberPredmet(int index);
	
	void uberZdravi(int kolik);
	int getSila();
	int getZdravi();
	std::string getJmeno();

	void naucInterakci(Interakce* interakce);
	void interaguj(Osoba* druhaOsoba);

};

#endif /* defined(__Command_Interaction__Osoba__) */
