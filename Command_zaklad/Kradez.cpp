//
// Created by xproch17 on 07.12.2018.
//

#include "Kradez.h"

Kradez::Kradez() : Interakce("Kradez"){

}

void Kradez::interaguj(Osoba* kdo, Osoba* skym){
    /// vypisu predmety u protivnika
    bool maPredmet = true;
    int pocitadlo = 0;
    while (maPredmet){
        Predmet* predmet = skym->getPredmet(pocitadlo);
        if (predmet == nullptr){
            maPredmet = false;
        } else {
            std::cout << "Jmeno predmetu [" << pocitadlo << "]: "
            << predmet->jmeno;
            pocitadlo++;
        }
    }

    if (pocitadlo == 0){
        std::cout << "Neni co krast!" << std::endl;
    } else {
        /// necham uzivatele vybrat predmet
        int rozhodnuti = Komunikace::ziskejRozhodnuti(pocitadlo - 1);
        /// pridam predmet do batohu
        Predmet *sebranejPredmet = skym->getPredmet(rozhodnuti);
        kdo->seberPredmet(sebranejPredmet);
        /// odeberu predmet od puvodniho majitele
        skym->odeberPredmet(rozhodnuti);
    }
}