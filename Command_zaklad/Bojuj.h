//
// Created by xproch17 on 07.12.2018.
//

#ifndef COMMAND_ZAKLAD_BOJUJ_H
#define COMMAND_ZAKLAD_BOJUJ_H

#include "Interakce.h"

class Bojuj:public Interakce {
public:
    Bojuj();
    void interaguj(Osoba* kdo, Osoba* skym);
};


#endif //COMMAND_ZAKLAD_BOJUJ_H
