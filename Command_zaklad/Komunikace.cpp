//
// Created by xproch17 on 07.12.2018.
//

#include "Komunikace.h"

int Komunikace::ziskejRozhodnuti(int maxHodnota){
    int rozhodnuti = 0;
    do {
        std::cout << "Zadej rozhodnuti: ";
        std::cin >> rozhodnuti;
    } while ((rozhodnuti < 0) or (rozhodnuti >= maxHodnota));

    return rozhodnuti;
}