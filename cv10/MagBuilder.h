//
// Created by xproch17 on 30.11.2018.
//

#ifndef CV10_MAGBUILDER_H
#define CV10_MAGBUILDER_H

#include "HrdinaBuilder.h"

class MagBuilder:public HrdinaBuilder {
public:
    MagBuilder();
    void createNewHrdina();
    void generujLektvary();
    void generujZbrane();
};


#endif //CV10_MAGBUILDER_H
