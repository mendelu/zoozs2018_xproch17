//
// Created by xproch17 on 30.11.2018.
//

#ifndef CV10_HRDINA_H
#define CV10_HRDINA_H

#include <iostream>
#include <vector>
#include "Zbran.h"
#include "Lektvar.h"

class Hrdina {
    std::string m_jmeno;
    std::string m_nazevPovolani;
    int m_obrana;
    int m_sila;
    std::vector<Zbran*> m_zbrane;
    std::vector<Lektvar*> m_lektvary;
public:
    Hrdina(std::string jmeno, std::string povolani,
            int sila, int obrana);
    void pridejZbran(Zbran* zbran);
    void pridejLektvar(Lektvar* lektvar);
    int getObrana();
    int getUtok();
    void vypijHorniLektvar();
    void printInfo();
};


#endif //CV10_HRDINA_H
