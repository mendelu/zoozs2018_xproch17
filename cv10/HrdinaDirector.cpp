//
// Created by xproch17 on 30.11.2018.
//

#include "HrdinaDirector.h"

HrdinaDirector::HrdinaDirector(HrdinaBuilder* builder){
    m_builder = builder;
}

void HrdinaDirector::setBuilder(HrdinaBuilder* builder){
    m_builder = builder;
}

Hrdina* HrdinaDirector::createHrdina(){
    m_builder->createNewHrdina();
    m_builder->generujLektvary();
    m_builder->generujZbrane();
    return m_builder->getHrdina();
}