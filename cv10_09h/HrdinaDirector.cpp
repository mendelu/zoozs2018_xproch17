//
// Created by xproch17 on 30.11.2018.
//

#include "HrdinaDirector.h"

HrdinaDirector::HrdinaDirector(HrdinaBuilder* builder){
    m_builder = builder;
}

void HrdinaDirector::setBuilder(HrdinaBuilder* builder){
    m_builder = builder;
}

Hrdina* HrdinaDirector::createHrdina(std::string jmeno){
    m_builder->createHrdina(jmeno);
    m_builder->generujZbrane();
    m_builder->generujLektvary();
    return m_builder->getHrdina();
}