//
// Created by xproch17 on 30.11.2018.
//

#ifndef CV10_09H_LEKTVAR_H
#define CV10_09H_LEKTVAR_H


class Lektvar {
    int m_bonusSily;
    int m_bonusObrany;
public:
    Lektvar(int bonusSily, int bonusObrany);
    int getBonusSily();
    int getBonusObrany();
};


#endif //CV10_09H_LEKTVAR_H
