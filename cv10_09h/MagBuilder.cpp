//
// Created by xproch17 on 30.11.2018.
//

#include "MagBuilder.h"

MagBuilder::MagBuilder(){

}

void MagBuilder::createHrdina(std::string jmeno){
    m_hrdina = new Hrdina(jmeno, "Mag", 10, 5);
}

void MagBuilder::generujZbrane(){
    Zbran* hulka = new Zbran("Magicka hulka", 5);
    m_hrdina->seberZbran(hulka);
}

void MagBuilder::generujLektvary(){
    Lektvar* lektvarSily = new Lektvar(15, 0);
    Lektvar* redBull = new Lektvar(20,20);
    Lektvar* vodka = new Lektvar(-10, -10);

    m_hrdina->seberLektvar(lektvarSily);
    m_hrdina->seberLektvar(redBull);
    m_hrdina->seberLektvar(vodka);
}
