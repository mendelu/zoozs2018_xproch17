//
// Created by xproch17 on 30.11.2018.
//

#ifndef CV10_09H_ZBRAN_H
#define CV10_09H_ZBRAN_H

#include <iostream>

class Zbran {
    std::string m_jmeno;
    int m_bonusSily;
public:
    Zbran(std::string jmeno, int bonusSily);
    std::string getJmeno();
    int getBonusSily();
};


#endif //CV10_09H_ZBRAN_H
