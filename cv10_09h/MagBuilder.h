//
// Created by xproch17 on 30.11.2018.
//

#ifndef CV10_09H_MAGBUILDER_H
#define CV10_09H_MAGBUILDER_H

#include "HrdinaBuilder.h"

class MagBuilder: public HrdinaBuilder {
public:
    MagBuilder();
    void createHrdina(std::string jmeno);
    void generujZbrane();
    void generujLektvary();
};


#endif //CV10_09H_MAGBUILDER_H
