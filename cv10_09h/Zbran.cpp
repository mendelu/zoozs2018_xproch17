//
// Created by xproch17 on 30.11.2018.
//

#include "Zbran.h"

Zbran::Zbran(std::string jmeno, int bonusSily){
    // TODO kontrola vstupu
    m_bonusSily = bonusSily;
    m_jmeno = jmeno;
}

std::string Zbran::getJmeno(){
    return m_jmeno;
}

int Zbran::getBonusSily(){
    return m_bonusSily;
}