//
// Created by xproch17 on 30.11.2018.
//

#ifndef CV10_09H_HRDINABUILDER_H
#define CV10_09H_HRDINABUILDER_H

#include "Hrdina.h"

class HrdinaBuilder {
protected:
    Hrdina* m_hrdina;
public:
    HrdinaBuilder();
    virtual void createHrdina(std::string jmeno) = 0;
    virtual void generujZbrane() = 0;
    virtual void generujLektvary() = 0;
    Hrdina* getHrdina();
};


#endif //CV10_09H_HRDINABUILDER_H
