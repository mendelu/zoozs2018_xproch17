#include <iostream>
using namespace std;

class Balik{
public:
    string m_odesilatel;
    string m_prijemce;
    float m_vaha;
    float m_cenaDopravy; // ne cena - cena pojisteni? cena baliku?

    Balik(string odesilatel, string prijemce,
               float vaha, float cenaDopravy){
        m_prijemce = prijemce;
        m_odesilatel = odesilatel;
        m_cenaDopravy = cenaDopravy;
        m_vaha = vaha;
    }

    Balik(string odesilatel, string prijemce){
        m_prijemce = prijemce;
        m_odesilatel = odesilatel;
        m_cenaDopravy = 0;
        m_vaha = 0;
    }

    void setVaha(float vaha){
        m_vaha = vaha;
    }

    void setCenaDopravy(float cenaDopravy){
        m_cenaDopravy = cenaDopravy;
    }

    string getOdesilatel(){
        return m_odesilatel;
    }

    string getPrijemce(){
        return m_prijemce;
    }

    float getVaha(){
        return m_vaha;
    }

    float getCenaDopravy(){
        return m_cenaDopravy;
    }

    void printInfo(){
        cout << "Prijemce: " << m_prijemce << endl;
        cout << "Odesilatel: " << m_odesilatel << endl;
        cout << "Vaha: " << m_vaha << endl;
        cout << "Cena: " << m_cenaDopravy << endl;
    }
};


int main() {
    Balik* proPepu = new Balik("David", "Pepa", 10.5, 150.5);
    proPepu->printInfo();

    Balik* proKarla = new Balik("David", "Karel");
    proKarla->printInfo();

    delete proKarla;
    delete proPepu;
    return 0;
}